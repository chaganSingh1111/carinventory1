
const carId = (inventory,id) => {
    let ans = []
    if (inventory == null || inventory == undefined){
        return ans
      }
    if (!Array.isArray(inventory)) {
        return ans
    }
    
    if (typeof id != 'number') {
        return ans
    }
    for(let idx = 0;idx < inventory.length;idx++){
        if(inventory[idx].id == id){
           
           ans[0] = inventory[idx]
           return ans
        }
        
    }
    return ans;
}

module.exports = carId;